package photosmanager;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Photo implements Comparable<Photo> {
	public File file;
	public Date acquisitionDate;
	
	public Photo (File f, Date acquisitionDate) {
		this.file = f;
		this.acquisitionDate = acquisitionDate;
	}
	
	public File getFile() {
		return file;
	}
	public Date getAcquisitionDate() {
		return acquisitionDate;
	}
	
	public int compareTo(Photo photo) {          
	    return (this.getAcquisitionDate().before(photo.getAcquisitionDate()) ? -1 : 
	            (this.getAcquisitionDate() == photo.getAcquisitionDate() ? 0 : 1));     
	}
	
	public String getPrefixFromDate () {
		System.out.println("Sto calcolando il prefisso da mettere");
		Date d = this.acquisitionDate;
		DateFormat format = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
		String dateString = format.format(d);
		System.out.println("check dateString oh Photo: " + dateString);
		String dayOfTheYear = dateString.split(" ")[0];
		String [] daySplit = dayOfTheYear.split(":");
		String yearComplete = daySplit[0];
		String year = yearComplete.substring(yearComplete.length() - 2);
		String month = daySplit[1];
		String day = daySplit[2];
		
		String prefixFromDate = year + "." + month + "." + day + "_";
		System.out.println("Il prefisso per la data e': " + prefixFromDate);
		return prefixFromDate;
	}


}
