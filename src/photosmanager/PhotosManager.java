package photosmanager;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.Parser;
import org.apache.commons.cli.PosixParser;

import org.apache.commons.lang3.StringUtils;

import com.thebuzzmedia.exiftool.ExifTool;
import com.thebuzzmedia.exiftool.ExifToolBuilder;
import com.thebuzzmedia.exiftool.Tag;
import com.thebuzzmedia.exiftool.core.StandardTag;

public class PhotosManager{
	
	public class TimeShifter{
		public int hours;
		public int minutes;
		public String device;
		
		public TimeShifter(String device, int hours, int minutes) {
			this.device = device;
			this.hours = hours;
			this.minutes = minutes;
		}
		
		public int getHours() {
			return hours;
		}

		public int getMinutes() {
			return minutes;
		}

		public String getDevice() {
			return device;
		}		
	}
	
	private Options options;
	private Parser parser;

    public static void main (String[] args) throws Exception {
    	
    	PhotosManager cl = new PhotosManager();
    	String filePath = null;
		Properties properties = null;
		//Integer maxItems = null;
		Map<String,Object> optMap = checkOptionList(args, cl);
		
		filePath = (String)optMap.get("propertiesPath");
		//Integer jobID = (Integer)optMap.get("jobID");
		//printSection("INIZIALIZZAZIONE");
		
		//LETTURA FILE PROPERTIES
		System.out.println("Lettura file di properties in corso...");
		properties = new Properties();
		InputStream is = null;
		try {
			is = new FileInputStream(filePath);
			properties.load(is);
		} catch(IOException e) {
		//	log.error("Errore nella lettura del file di properties [" + filePath + "]");
			System.exit(0);
		} finally {
			if(is != null) {
				try {
					is.close();
				} catch(Exception e){}
			}
		}
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Hai settato correttamente il prefix_name? S/N");
    	String ok = scan.nextLine();
    	if(!("S".equals(ok))) {
    		System.exit(0);    	    		
    	}		
		// LETTURA FILE DI PROPERTIES
        String pathFolder = properties.getProperty(Constants.FOLDERPATH);
        String pathExif = properties.getProperty(Constants.PATHEXIFTOOL);
        String namePhotosPrefix = properties.getProperty(Constants.NAMEPHOTOS_PREFIX);
        //System.out.println("Folder path dal file di prop: <<" + pathFolder + ">>");
        File folder = new File(pathFolder);
        File[] listOfFiles = folder.listFiles();
       // String namePhotos_prefix = "20.03.08_Grappa_";
		// String namePhotos_date = "20.03.08_";
        Utils utils = new Utils(pathExif);
        //get all devices of photos in the folder 
        Set<String> devices = utils.getDevices(pathFolder);
        Scanner s = new Scanner(System.in);
        System.out.println("Dev SET:\n" + devices.toString());
        List<TimeShifter> timeShifters = new ArrayList<TimeShifter>();
        
        for (Iterator<String> it = devices.iterator(); it.hasNext();) {
            String dev = it.next();
            if(StringUtils.equals(dev, "null") || dev==null) {
            	System.out.println("Ho trovato un dispositivo NULL");
            	System.exit(0);
            }
            else {
            	System.out.println("Di quante ore vuoi shiftare l'orario di acquisizione per le foto scattate da: " + dev);
            	String inputHours = s.nextLine();
           		System.out.println("Di quanti minuti vuoi shiftare l'orario di acquisizione per le foto scattate da: " + dev);
            	String inputMinutes = s.nextLine();
            	int hours = 0;
            	int minutes = 0;
            	try {
            		hours = Integer.parseInt(inputHours);
            		minutes = Integer.parseInt(inputMinutes);
            		System.out.println("OK, shifto di " + hours + " ore e " + minutes + " minuti");
            	}catch(NumberFormatException nfe) {
            		System.out.println(nfe.getMessage());
            		System.exit(0);
            	}
            	
            	if(!(hours==0 && minutes==0)) {
            		//System.out.println("Dev: " + dev);
            		//System.out.println("Hours: " + hours);
            		//System.out.println("Minutes: " + minutes);
            		TimeShifter ts = cl.new TimeShifter(dev, hours, minutes);
            		timeShifters.add(ts);
            	}
            }
        }
        s.close();
        System.out.println("TimeShifters length: " + timeShifters.size());
        utils.changeDate(pathFolder, timeShifters, namePhotosPrefix);
    }
    
    
private static Map<String,Object> checkOptionList(String[] args, PhotosManager cl) {
		
		Map<String,Object> optMap = new HashMap<String,Object>();
		try {
			//log.info("Acquisizione delle opzioni del programma in corso...");
		     CommandLine line = cl.getParser().parse( cl.getOptions(), args );
		     if(line.hasOption("h")){
		       HelpFormatter formatter = new HelpFormatter();
		       formatter.printHelp( "java -jar ZZZ.jar [<OPTIONS>] ", cl.getOptions() );
		       System.exit(0);
		     }
		     		     
		     if(line.hasOption("f")){
		    	 String filePath = line.getOptionValue("f");
		    	 //log.info("filePath: [" + filePath + "]");
		    	 try {
		    		 checkIsFile(filePath);
		    	 } catch(Exception e) {
		    		 //log.error("Il percorso indicato non � associato ad un file");
		    		 System.out.println("Il percorso indicato non � associato ad un file");
		    		 HelpFormatter formatter = new HelpFormatter();
				     formatter.printHelp( "java -jar ZZZ.jar <OPTIONS> ", cl.getOptions());
				     System.exit(0);
		    	 }
		    	 optMap.put("propertiesPath", filePath);
			 }
		     
		     
		   } catch (ParseException e1) {
		       HelpFormatter formatter = new HelpFormatter();
		       formatter.printHelp( "java -jar ZZZ.jar <OPTIONS> ", cl.getOptions());
		       System.exit(0);
		   }
		return optMap;
	}


	private static void checkIsFile(String filePath) throws Exception {
		File f = new File(filePath);
		if(f == null) { 
			throw new Exception("File configurazione � null");
		} else if(!f.isFile()) {
			throw new Exception("Il percorso non � relativo a un file");
		}
	}

	public static Options getOptionsInstance() {
		PosixParser parser = new PosixParser();

		// create the Options
		Options options = new Options();
		options.addOption("h", false, "Stampa questo help");
		
		// file di configurazione con i riferimenti a DB, WCC, user, pwd
		options.addOption(OptionBuilder
				.withDescription("Obbligatorio, file di config: deve essere *.(config|properties)")
				.hasArg().withArgName("CONFIG FILE").isRequired(true)
				.create("f"));
		/*
		// numero massimo di item da processare (per tipologia)
		options.addOption(OptionBuilder
				.withDescription("Numero di elementi massimo da processare")
				.hasArg().withArgName("#ITEMS").withType(Integer.class)
				.create("n"));
		*/
		return options;
	}
	
	public Options getOptions() {
		if (options == null) {
			options = getOptionsInstance();
		}
		return options;
	}
	
	public Parser getParser() {
		if (parser == null) {
			parser = new PosixParser();
		}
		return parser;
	}

}
