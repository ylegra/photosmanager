package photosmanager;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.thebuzzmedia.exiftool.ExifTool;
import com.thebuzzmedia.exiftool.ExifToolBuilder;
import com.thebuzzmedia.exiftool.Tag;
import com.thebuzzmedia.exiftool.core.StandardTag;

import photosmanager.PhotosManager.TimeShifter;

public class Utils {
	public static String pathExif;
	
	public Utils (String pathExif){
		this.pathExif = pathExif;
	}
	
	public void changeDate(String pathFolder, List<TimeShifter> tsList, String prefixName) throws IOException, ParseException {
		try {
			File folder = new File(pathFolder);
		    File[] listOfFiles = folder.listFiles();
		    List<Photo> photoList = new ArrayList<Photo>();
		    
		    for (int i = 0; i < listOfFiles.length; i++) {
				File file = listOfFiles[i];
				
				ExifTool tool = new ExifToolBuilder()
				         .withPath(pathExif)
				         .build();
		
				List<Tag> tags = new ArrayList<Tag>();
				//tags.add(StandardTag.CREATE_DATE);
				tags.add(StandardTag.DATE_TIME_ORIGINAL);
				tags.add(StandardTag.MODEL);
				
				Map<Tag, String> valueMap = tool.getImageMeta(file, tags);
				if(valueMap.get(StandardTag.DATE_TIME_ORIGINAL)!=null) {
					
					String fileDev = valueMap.get(StandardTag.MODEL);
					
					int deltaHours = 0;
					int deltaMinutes = 0;
					for(int j = 0; j<tsList.size(); j++) {
						if(fileDev.contentEquals(tsList.get(j).getDevice())){
							deltaHours = tsList.get(j).getHours();
							deltaMinutes = tsList.get(j).getMinutes();
						}
					}
					
					//
					//System.out.println("Create Date: " + valueMap.get(StandardTag.CREATE_DATE));
					System.out.println("Data originale --> " + valueMap.get(StandardTag.DATE_TIME_ORIGINAL));
					
					String originalDateString = valueMap.get(StandardTag.DATE_TIME_ORIGINAL);
					DateFormat format = null;
					format = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");// HH:mm:ss");
					Date date = format.parse(originalDateString);
					if(!(deltaHours==0 && deltaMinutes==0)) {
						
						//System.out.println(date); 
						
						Calendar cal = Calendar.getInstance();
						// remove next line if you're always using the current time.
						cal.setTime(date);
						//cal.add(Calendar.HOUR, -1);
						//cal.add(Calendar.MINUTE, -9);
						cal.add(Calendar.HOUR, deltaHours);
						cal.add(Calendar.MINUTE, deltaMinutes);
						Date newDate = cal.getTime();
						//System.out.println(newDate);
						
						
						List<Tag> newTags = new ArrayList<Tag>();
						String newDateString = format.format(newDate);
						System.out.println("Data modificata --> " + newDateString);
						Map<Tag, String> newValueMap = new HashMap <Tag,String>();
						newValueMap.put(StandardTag.DATE_TIME_ORIGINAL, newDateString);
						//tool.setImageMeta(file, newValueMap); //Commentando questa ordina ma non modifica la data di acquisizione
						
						Photo ph = new Photo (file, newDate);
						photoList.add(ph);
					//
					}else {
						valueMap.get(StandardTag.DATE_TIME_ORIGINAL);
						Photo ph = new Photo (file, date);
						photoList.add(ph);
					}
					
					
					
				}
				else {
					System.out.println("ATTENZIONE: CI SONO FILE CON ACQUISITION DATE = NULL");
				}
				
		    }
		    photoList = getSortedPhotos(photoList);
		    renameFiles(photoList, pathFolder, prefixName);				
				
		} catch(IOException ioe) {
			System.out.println(ioe.getMessage());
		} catch (ParseException pe) {
			System.out.println(pe.getMessage());
		}
		
		
	}
	
	public Set<String> getDevices (String pathFolder) {
		long start = System.currentTimeMillis();
		System.out.println("Sto recuperando i dispositivi presenti nella cartella...");
		Set<String> devices = new HashSet<String>();
		try {
			File folder = new File(pathFolder);
		    File[] listOfFiles = folder.listFiles();
		    for (int i = 0; i < listOfFiles.length; i++) {
	            if (listOfFiles[i].isFile()) {
	            	File f = listOfFiles[i];
					
					ExifTool tool = new ExifToolBuilder()
					         .withPath(pathExif)
					         .build();
			
					List<Tag> tags = new ArrayList<Tag>();
					tags.add(StandardTag.MODEL);
					
					Map<Tag, String> valueMap = tool.getImageMeta(f, tags);
					//System.out.println("Device i: " + valueMap.get(StandardTag.MODEL));
					devices.add(valueMap.get(StandardTag.MODEL));					
				}
		    }
		    
		}catch(IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		long end = System.currentTimeMillis();
		long minuti = (end - start) / 60000;
		long secondi = ((end - start) - minuti * 60000) / 1000;
		System.out.println("Tempo impiegato e' " + minuti + ":" + secondi);
		return devices;
	}

	public void renameFiles(List<Photo> photoList, String pathFolder, String prefixName) {
		 for (int i = 0; i < photoList.size(); i++) {
	            if (photoList.get(i).getFile().isFile()) {
	                String nameFile = photoList.get(i).getFile().getName();
	                File f = new File (pathFolder + "\\" + nameFile);
					String extension = isImage(nameFile);
					
					System.out.println("Nome originale --> " + nameFile);
					
					int numberInt = i+1;
					if(extension!=null && !("".equals(extension))){	
						String prefix = photoList.get(i).getPrefixFromDate() + prefixName;
	                        try {
	                            File newFile = new File(pathFolder + "\\" + prefix + "_" + String.format("%03d", numberInt) + extension);
	                            if(f.renameTo(newFile)) {
	                               // System.out.println("File rename success");
	                            }else{
									//System.out.println("Number " + numberInt);
									System.out.println("File rename insuccess");
								}
	                        } catch(Exception e){
	                            System.out.println(e.getMessage());
	                        }
	                }
					
	            } else if (photoList.get(i).getFile().isDirectory()) {
	                System.out.println("Directory " + photoList.get(i).getFile().getName());
	            }
	           
	        }
		 

	   }
		
		public static String isImage(String nameFile){
			String extension = "";
			if(nameFile.contains(".JPG") || nameFile.contains(".jpg") || nameFile.contains(".jpeg")) {
				if(nameFile.endsWith(".JPG"))
					extension = ".JPG";
				else if(nameFile.endsWith(".jpg"))
					extension = ".jpg";
				else if(nameFile.endsWith(".jpeg"))
					extension = ".jpeg";
			}
			return extension;
		}
		
		public List<Photo> getSortedPhotos(List<Photo> orderedPhotos) { 
		    Collections.sort(orderedPhotos);         
		    return orderedPhotos;     
		  } 
	
}
